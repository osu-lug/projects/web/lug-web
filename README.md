# Running Locally for Development

1. Install [Hugo Static Site Generator](https://gohugo.io/)
2. Clone this repo
  ```bash
  $ git clone https://gitlab.com/osu-lug/projects/web/lug-web.git
  ```
3. Start the webserver
  ```bash
  # change into directory
  $ cd ./lug-web/
  # start
  $ hugo server -w
  # The server will now live-update each page as you save changes to files.
  ```

# Contributing

Check out the listed issues available that need to be worked on, and pick one up if you so choose. You may also contribute unlisted content, which will go through a review process.

## Creating a page

Make sure the page you want to create does not already exist. Check in content/ and layouts/. If it does not, then run:

`hugo new [PATH]`

Example:

```bash
$ hugo new getting-started
# creates a page at https://lug.oregonstate.edu/getting-started. Markdown to configure available at content/getting-started.md

$ hugo new blog/hello-world
# creates a blog/page at https://lug.oregonstate.edu/blog/hello-world. Themeing based on archetype at archetypes/blog.md. Markdown for content available at content/blogs/hello-world.md.
```

## Editing a page

Order of operations to add content to a page:

1. In general, pages should try to use mostly markdown, if possible. Make exceptions at your discression. If you do not know markdown very well, while editing check out this reference: https://www.markdownguide.org/cheat-sheet

2. If what you are trying to achieve is not available through markdown, check if a shortcode is available for what you are trying to achieve. First, consult the default Hugo shortcodes, which you can reference here: https://gohugo.io/content-management/shortcodes/ Next, if there is no built-in hugo version, check if there is already a custom one in layouts/shortcodes. There are also built-in hugo shortcodes, If there is no shortcode, proceed to the next step.

3.  If there is no markdown or shortcode for your goal, consult the available utilities of our theme, Kube. These options are available here: https://kube.elemnts.net/docs/. To use the HTML, create shortcodes then import as shortcodes in the markdown file. To create a shortcode, follow the Hugo guide: https://gohugo.io/templates/shortcode-templates/. This MUST be done in a shortcode.

4. Next, if what you are trying to achieve is not available in markdown, shortcodes, or theme, you may create your own custom HTML.

5. If what you are trying to achieve is still not achievable through shortcodes, you may create a fully custom HTML layout in layouts/[page] for this page. If you want to load in the associated markdown content, make sure to use some tags like `{{ .Content }}` to load it in to the HTML.

## Side Notes

### Adding links

When adding a link, care must be taken. For example, if you are wishing to have a link to `{BaseURL}/contact/` by making a link reference to `href="/contact/"`, the extra '/' at the end is required. Removing the extra '/' at the end and making a link to something like `href="/contact"` will break links in production, but they will still work locally. This is a side effect of nginx.
