---
title: "About"
date: 2020-09-28T23:40:35Z
draft: false
---
We at the Linux Users Group are a group of passionate supporters of Free & Open Source Software (FOSS). We are a highly active and self-organizing group dedicated to teaching and advocating free and open source software (FOSS) on campus.

Our approach is simple. Members join either because they are new users and would like help, or because they are active users and want to help promote FOSS. Because of the infectious nature of open source, the group nearly builds itself.

Since 2004 we have benefited the Oregon State student body by providing free support for Linux, increasing awareness about the open source revolution, and jointly creating events with other groups on campus. We share many members with the OSU Open Source Education Lab and are hosted by the OSU Open Source Lab.

We have meetings every Tuesday at 6:00 pm in Kelley Engineering Center room 1005. All students and community members are welcome to attend.

_(This page is a work in progress!)_
