---
title: "OSU Linux Users Group"
description : "Oregon State University"
date: 2020-09-27T23:31:42Z
draft: false
---

### What is the Linux Users Group (LUG)?

LUG is an organization which celebrates and supports the use of Linux and Free & Open Source Software (FOSS).

### Who can join LUG?

Anyone can join LUG! Most of our members are OSU students, however anyone, even those not affiliated with Oregon State
University, may participate!

### How do I participate?

Check out our in-depth Getting Started guide, which goes in to how to get started with participating in LUG.

##### Mailing List

Sign up for weekly emails and discussions via email. All major LUG announcements are sent through this mailing list, so
we recommend even those interested to join.

[Mailing List Signup](https://lists.oregonstate.edu/mailman/listinfo/linux)

##### Online Community

[Matrix](https://docs.lug.oregonstate.edu/LUG/): LUG runs a self-hosted Matrix homeserver that anyone can join. We have
a homeserver for confirmed LUG members available, but feel free to join from any homeserver.

[IRC](https://docs.lug.oregonstate.edu/LUG/): LUG has a server on Libera Chat, which is bridged to our Matrix server. If
you prefer to communicate over IRC, please join `#osu-lug` on irc.libera.chat.

[Discord & Other Options](https://docs.lug.oregonstate.edu/LUG/) - If you prefer to not use either of the above options
for communication, we understand that committing to using new software is not always desirable. While Discord is not
Open Source software, there is value in being accessible and having a platform to talk with people who are not prepared
for the jump into going fully Open Source yet. Discord is bridged to Matrix so you won't miss anything!

##### Weekly Meetings - Tuesdays @ 6pm

We meet every Tuesdays in-person at 6pm in Kelley Engineering Center room 1005 on the Oregon State University campus.
**However, due to the current levels of COVID on campus we are meeting remote for now on Jitsi.**

We will announce on the mailing list what the topic of the upcoming week's meeting is about, and you can decide whether
or not to participate.

If you are working on a project that you wanting to share during a meeting, [please reach out to us](/contact/). Whether
that be your journey on installing Gentoo on an Android, your home lab, setting up a multi-gpu system, or any other fun
thing you have been working on.
