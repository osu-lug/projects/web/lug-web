---
title: "Weekly Meetings"
date: 2020-09-27T23:31:42Z
draft: false
---


Weekly on **Tuesdays at 6pm** in **Kelly 1005**.

We also mirror the meetings online via Jitsi: [**join in your browser**](https://meet.jit.si/WeeklyLUG)
(join one of our social platforms for the password)

_(This page is a work in progress!)_
