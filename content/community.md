---
title: "Online Community"
date: 2020-09-27T23:31:42Z
draft: false
---

[Matrix](https://docs.lug.oregonstate.edu/LUG/): LUG runs a Matrix Organization, that anyone can join. We have a homeserver for confirmed LUG members available, but feel free to join from any homeserver.

[IRC](https://docs.lug.oregonstate.edu/LUG/): LUG has a server on Libera Chat, which is bridged to our Matrix server. If you prefer to communicate over IRC, please join `#osu-lug` on irc.libera.chat.

[Discord & Other Options](https://docs.lug.oregonstate.edu/LUG/) - If you prefer to not use either of the above options for communication, we understand that committing to using new software is not always desirable. While Discord is not Open Source software, there is value in being accessible and having a platform to talk with people who are not prepared for the jump into Open Source yet. Thanks to the Matrix Discord bridge, chat can be synced.

_(This page is a work in progress!)_